import numpy as np

def relu(Z):
    return np.multiply(Z,(Z>0))

def sigmoid(Z):
    y = np.exp(Z)
    return y/(1+y)

def dsigmoid(A):
    return np.multiply(A,(1-A))

def drelu(A):
    return (A>0)

class NeuralNetwork:

    def __init__(self, hyperparameters):
        self.layer_dims, self.learning_rate, self.iterations = hyperparameters
        self.parameters = self.random_initialization()
        self.layers = len(self.parameters)//2

    def random_initialization(self):
        parameters = {}
        for layer in range(1, len(self.layer_dims)):
            parameters['W'+str(layer)] = np.random.randn(self.layer_dims[layer],self.layer_dims[layer-1])*0.01
            parameters['b'+str(layer)] = np.zeros((self.layer_dims[layer],1))
        return parameters

    def forward_activation(self,A_prev,W,b,func):
        Z = np.dot(W,A_prev)+b
        A = func(Z)
        cache = (A_prev, A)
        return A, cache

    def forward_propagation(self,X):
        A, caches = X, []
        for layer in range(1,self.layers):
            A, cache = self.forward_activation(A, self.parameters['W'+str(layer)],self.parameters['b'+str(layer)],relu)
            caches.append(cache)
        AL, cache = self.forward_activation(A, self.parameters['W'+str(self.layers)],self.parameters['b'+str(self.layers)],sigmoid)
        caches.append(cache)
        return AL, caches

    def cost_function(self,AL,Y, m):
        return -(Y*np.log(AL)+(1-Y)*np.log(1-AL)).sum()/m
    
    def backward_activation(self,dA,cache,W,m,func):
        linear_cache, activation_cache = cache
        dZ = dA * func(activation_cache)
        dW = np.dot(dZ, linear_cache.T)/m
        db = dZ.sum(axis=1, keepdims=True)/m
        dA_p = np.dot(W.T,dZ)
        return dA_p, dW, db

    def backward_propagation(self,AL,Y,caches,m):
        grads = {}
        dAL=-(np.divide(Y,AL)-np.divide(1-Y,1-AL))
        grads["dA" + str(self.layers-1)], grads["dW" + str(self.layers)], grads["db" + str(self.layers)] = self.backward_activation(dAL, caches[self.layers-1], self.parameters["W"+str(self.layers)], m, dsigmoid)
        for layer in reversed(range(1,self.layers)):
            grads["dA" + str(layer-1)] , grads["dW" + str(layer)], grads["db" + str(layer)] = self.backward_activation(grads["dA" + str(layer)], caches[layer-1], self.parameters["W"+str(layer)],m,drelu)
        return grads

    def update_parameters(self, grads):
        for layer in range(self.layers):
            self.parameters["W" + str(layer+1)] += - self.learning_rate * grads["dW" + str(layer+1)]
            self.parameters["b" + str(layer+1)] += - self.learning_rate * grads["db" + str(layer+1)]


    def train(self,X,Y,print_cost=False):
        m = X.shape[1]
        for i in range(self.iterations):
            AL, caches = self.forward_propagation(X)
            grads = self.backward_propagation(AL,Y,caches,m)
            self.update_parameters(grads)
            if print_cost and i%100==0:
                print(self.cost_function(AL,Y,m))

    def predict(self,X):
        AL, _ = self.forward_propagation(X)
        print(AL)

X = np.array([[1,2,3],[3,2,1],[4,5,6]])
Y = np.array([1,0,0])
net = NeuralNetwork(([3,4,2],0.1,1000))
net.train(X.T,Y,True)
