import numpy as np
import pandas as pd

def relu(Z):
    
    return np.multiply(Z,(Z>0))


def sigmoid(Z):

    y = np.exp(Z)
    return y/(1+y)


def dsigmoid(A):

    return np.multiply(A,(1-A))


def drelu(A):

    return (A>0)


class NeuralNetwork:

    def __init__(self, layers_dims, iterations = 1000, learning_rate = 0.1, B_1 = 0.9, B_2 = 0.999, epsilon = 1e-8, batch_size = 4):
        
        self.layer_dims, self.iterations, self.learning_rate, self.batch_size = layers_dims, iterations, learning_rate, batch_size
        self.B_1, self.B_2, self.epsilon = B_1, B_2, epsilon
        self.hyperparameters, self.parameters = self.random_initialization()
        self.layers = len(self.parameters)//2


    def random_initialization(self):
        
        hyperparameters, parameters = {}, {}
        
        for layer in range(1, len(self.layer_dims)):
            
            parameters['W'+str(layer)] = np.random.randn(self.layer_dims[layer],self.layer_dims[layer-1])*np.sqrt(np.divide(2,self.layer_dims[layer-1]))
            hyperparameters['VdW'+str(layer)], hyperparameters['SdW'+str(layer)] = 0, 0 
            parameters['b'+str(layer)] = np.zeros((self.layer_dims[layer],1))
            hyperparameters['Vdb'+str(layer)], hyperparameters['Sdb'+str(layer)] = 0, 0
            
        return (hyperparameters, parameters)


    def forward_activation(self, A_prev, W, b, func):
        
        Z = np.dot(W,A_prev)+b
        A = func(Z)
        cache = (A_prev, A)
        
        return A, cache


    def forward_propagation(self, X):
        
        A, caches = X, []
        
        for layer in range(1,self.layers):
            
            A, cache = self.forward_activation(A, self.parameters['W'+str(layer)],self.parameters['b'+str(layer)],relu)
            caches.append(cache)
            
        AL, cache = self.forward_activation(A, self.parameters['W'+str(self.layers)],self.parameters['b'+str(self.layers)],sigmoid)
        caches.append(cache)
        
        return AL, caches


    def cost_function(self, AL, Y, m):
        
        return -(Y*np.log(AL)+(1-Y)*np.log(1-AL)).sum()/m

    
    def backward_activation(self,dA,cache,W,m,func):
        
        linear_cache, activation_cache = cache
        dZ = dA * func(activation_cache)
        dW = np.dot(dZ, linear_cache.T)/m
        db = dZ.sum(axis=1, keepdims=True)/m
        dA_p = np.dot(W.T,dZ)
        
        return dA_p, dW, db


    def backward_propagation(self, AL, Y, caches, m):
        
        grads = {}
        dAL= -(np.divide(Y,AL)-np.divide(1-Y,1-AL))
        grads["dA" + str(self.layers-1)], grads["dW" + str(self.layers)], grads["db" + str(self.layers)] = self.backward_activation(dAL, caches[self.layers-1], self.parameters["W"+str(self.layers)], m, dsigmoid)

        for layer in reversed(range(1,self.layers)):
            
            grads["dA" + str(layer-1)] , grads["dW" + str(layer)], grads["db" + str(layer)] = self.backward_activation(grads["dA" + str(layer)], caches[layer-1], self.parameters["W"+str(layer)],m,drelu)

        return grads


    def update_parameters(self, grads, t):
        
        for layer in range(self.layers):

            self.hyperparameters["VdW" + str(layer+1)] = self.B_1 * self.hyperparameters["VdW" + str(layer+1)] + (1-self.B_1) * grads["dW" + str(layer+1)]
            self.hyperparameters["Vdb" + str(layer+1)] = self.B_1 * self.hyperparameters["Vdb" + str(layer+1)] + (1-self.B_1) * grads["db" + str(layer+1)]
            self.hyperparameters["SdW" + str(layer+1)] = self.B_2 * self.hyperparameters["SdW" + str(layer+1)] + (1-self.B_2) * np.square(grads["dW" + str(layer+1)])
            self.hyperparameters["Sdb" + str(layer+1)] = self.B_2 * self.hyperparameters["Sdb" + str(layer+1)] + (1-self.B_2) * np.square(grads["db" + str(layer+1)])
            
            vdw_corrected = np.divide(self.hyperparameters["VdW" + str(layer+1)], 1 - np.power(self.B_1,t))
            vdb_corrected = np.divide(self.hyperparameters["Vdb" + str(layer+1)], 1 - np.power(self.B_1,t))
            sdw_corrected = np.divide(self.hyperparameters["SdW" + str(layer+1)], 1 - np.power(self.B_2,t))
            sdb_corrected = np.divide(self.hyperparameters["Sdb" + str(layer+1)], 1 - np.power(self.B_2,t))
                        
            self.parameters["W" + str(layer+1)] += - self.learning_rate * np.divide(vdw_corrected, np.sqrt(sdw_corrected)+ self.epsilon)
            self.parameters["b" + str(layer+1)] += - self.learning_rate * np.divide(vdb_corrected, np.sqrt(sdb_corrected)+ self.epsilon)


    def getBatches(self, m):
        
        n_batches = (m // self.batch_size) if (m % self.batch_size) == 0 else (m // self.batch_size) + 1  
        batch_indexes = [(i*self.batch_size,((i+1)*self.batch_size)-1) for i in range(n_batches)]
        batch_indexes.append((batch_indexes.pop()[0], m-1))
        
        return batch_indexes


    def train(self, X, Y, print_cost=False):
        
        m = X.shape[1]
        y = self.transform_classes(Y)
        batches = self.getBatches(m)
        
        for i in range(self.iterations):            
            for (lower, upper) in batches:

                AL, caches = self.forward_propagation(X[:,lower:upper])
                grads = self.backward_propagation(AL,y[:,lower:upper],caches,m)
                self.update_parameters(grads, i+1)
                
            if print_cost and i%100==0:
                print(self.cost_function(self.forward_propagation(X)[0],Y,m))


    def transform_classes(self, Y):
        
        y_n = np.zeros((Y.shape[0], self.layer_dims[self.layers]))
        
        for i in range(Y.shape[0]):
            
            y_n[i][Y[i]-1] = 1
            
        return y_n.T


    def predict(self, X):
        
        AL, _ = self.forward_propagation(X)
        print(AL)
