import numpy as np

def relu(Z):
    return np.multiply(Z,(Z>0))

def sigmoid(Z):
    y = np.exp(Z)
    return y/(1+y)

def dsigmoid(A):
    return np.multiply(A,(1-A))

def drelu(A):
    return (A>0)

class NeuralNetwork:

    def __init__(self, layers_dims, learning_rate, iterations, lambd, keep_chance):
        self.layer_dims, self.learning_rate, self.iterations, self.lambd, self.keep_chance = layers_dims, learning_rate, iterations, lambd, keep_chance
        self.parameters = self.random_initialization()
        self.dropout_mask = {}
        self.layers = len(self.parameters)//2

    def random_initialization(self):
        parameters = {}
        for layer in range(1, len(self.layer_dims)):
            parameters['W'+str(layer)] = np.random.randn(self.layer_dims[layer],self.layer_dims[layer-1])*np.sqrt(np.divide(2,self.layer_dims[layer-1]))
            parameters['b'+str(layer)] = np.zeros((self.layer_dims[layer],1))
        return parameters

    def forward_activation(self,A_prev,W,b,func):
        Z = np.dot(W,A_prev)+b
        A = func(Z)
        cache = (A_prev, A)
        return A, cache

    def forward_propagation(self,X):
        A, caches = X, []
        for layer in range(1,self.layers):
            A, cache = self.forward_activation(A, self.parameters['W'+str(layer)],self.parameters['b'+str(layer)],relu)
            D = (np.random.rand(*A.shape) < self.keep_chance[layer-1]).astype(int)
            A = np.divide(A*D,self.keep_chance[layer-1])
            self.dropout_mask['D'+str(layer)] = D
            caches.append(cache)
        AL, cache = self.forward_activation(A, self.parameters['W'+str(self.layers)],self.parameters['b'+str(self.layers)],sigmoid)
        caches.append(cache)
        return AL, caches

    def cost_function(self,AL,Y, m):
        return -(Y*np.log(AL)+(1-Y)*np.log(1-AL)).sum()/m + np.divide(self.lambd,2*m) * np.array([np.square(self.parameters['W'+str(i)]).sum() for i in range(1,self.layers)]).sum()
    
    def backward_activation(self,dA,cache,W,m,func):
        linear_cache, activation_cache = cache
        dZ = dA * func(activation_cache)
        dW = np.dot(dZ, linear_cache.T)/m + (self.lambd/m * W)
        db = dZ.sum(axis=1, keepdims=True)/m
        dA_p = np.dot(W.T,dZ)
        return dA_p, dW, db

    def backward_propagation(self,AL,Y,caches,m):
        grads = {}
        dAL=-(np.divide(Y,AL)-np.divide(1-Y,1-AL))
        grads["dA" + str(self.layers-1)], grads["dW" + str(self.layers)], grads["db" + str(self.layers)] = self.backward_activation(dAL, caches[self.layers-1], self.parameters["W"+str(self.layers)], m, dsigmoid)
        for layer in reversed(range(1,self.layers)):
            dA = grads["dA" + str(layer)] * self.dropout_mask['D'+str(layer)]/self.keep_chance[layer-1]
            grads["dA" + str(layer-1)] , grads["dW" + str(layer)], grads["db" + str(layer)] = self.backward_activation(dA, caches[layer-1], self.parameters["W"+str(layer)],m,drelu)
        return grads
    
    def update_parameters(self, grads, m):
        for layer in range(self.layers):
            self.parameters["W" + str(layer+1)] += - (self.lambd*self.learning_rate/m) - self.learning_rate * grads["dW" + str(layer+1)]
            self.parameters["b" + str(layer+1)] += - self.learning_rate * grads["db" + str(layer+1)]

    def train(self,X,Y,drop_out=False,print_cost=False):
        m = X.shape[1]
        Y = self.__transform_Y(Y)
        for i in range(self.iterations):
            AL, caches = self.forward_propagation(X)
            grads = self.backward_propagation(AL,Y,caches,m)
            self.update_parameters(grads,m)
            if print_cost and i%100==0:
                print(self.cost_function(AL,Y,m))

    def __transform_Y(self, Y):
        Yn = np.zeros((Y.shape[0], self.layer_dims[self.layers]))
        for i in range(Y.shape[0]):
            Yn[i][Y[i]-1] = 1
        return Yn.T
                    
    def predict(self,X):
        AL, _ = self.forward_propagation(X)
        print(AL)

X = np.array([[1,2,3],[3,2,1],[4,5,6],[6,7,8],[1,4,5],[1,1,1],[0,5,3],[-1,3,2]])
Y = np.array([1,0,0,1,0,0,1,1])

net = NeuralNetwork([3,3,3,3,1],0.01,10000,0,[1,1,1])
net.train(X.T,Y,False,True)
